package qa.tests;

import com.persado.oss.quality.stevia.selenium.core.WebComponent;

public class AmazonPageObject extends WebComponent{

	public enum Locators {
		BUTTON_AMAZON_TODAYSDEALS("css=a[text='Today's Deals']"),
		BUTTON_AMAZON_SEE_DETAILS("css=a[text='See details']"),
		BUTTON_AMAZON_3RD_ITEM("css=div[class='a-link-normal']"),
		BUTTON_AMAZON_ADD_TO_CART("css=input[value='Add to Cart']"),
		OUTPUT_AMAZON_PRODUCT_PRICE("css=span[id='attach-accessory-cart-subtotal']"),
		BUTTON_AMAZON_GO_TO_CART("css=span[text='Cart']"),
		OUTPUT_AMAZON_PRODUCT_PRICE_IN_CART("css=span[id='sc-subtotal-amount-buybox']")
		;


		private String myLocator;

		Locators(String locator){
			myLocator = locator;
		}

		public String get() {
			return myLocator;
		}
	}

	public void pressTodaysDealsButton() {
		controller().press(Locators.BUTTON_AMAZON_TODAYSDEALS.get());
	}

	public void pressSeeDetailsButton() {
		controller().press(Locators.BUTTON_AMAZON_SEE_DETAILS.get());
	}

	public void press3rdItem() {
		int i = 0;
		//for loop in list of elements with the same class name, in our case in the same item category
		while(Locators.BUTTON_AMAZON_3RD_ITEM.getClass().toString().equalsIgnoreCase("a-link-normal") && i<=3) {
			if (i == 3) {
				controller().press(Locators.BUTTON_AMAZON_3RD_ITEM.get());
				break;
			}
			i++;

		}

	}

	public void pressAddToCart() {
		controller().press(Locators.BUTTON_AMAZON_ADD_TO_CART.get());
	}
	
	public String getProductPrice() {
		return controller().getText(Locators.OUTPUT_AMAZON_PRODUCT_PRICE.get());
	}
	
	public void pressCart() {
		controller().press(Locators.BUTTON_AMAZON_GO_TO_CART.get());
	}
	
	public String getProductPriceInCart() {
		return controller().getText(Locators.OUTPUT_AMAZON_PRODUCT_PRICE_IN_CART.get());
	}
}
