package qa.tests;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.persado.oss.quality.stevia.spring.SteviaTestBase;

public class AmazonPriceVerification extends SteviaTestBase{


	@Autowired
	protected AmazonPageObject amazonPageObject;

	@BeforeClass
	public void Preconditions() throws InterruptedException {
		amazonPageObject.pressTodaysDealsButton();
		//No action for about 2 seconds in order to let the browser load the current function
		Thread.sleep(2000);
		//1st listed product/category
		amazonPageObject.pressSeeDetailsButton();
		Thread.sleep(2000);

		amazonPageObject.press3rdItem();
		Thread.sleep(2000);

		//Add product/item to cart 
		amazonPageObject.pressAddToCart();
		Thread.sleep(2000);

	}

	@Test
	public void PriceVerification() throws InterruptedException {
		//Get product price
		String productPrice = amazonPageObject.getProductPrice();

		//Go to basket
		amazonPageObject.pressCart();
		Thread.sleep(2000);

		//Get product price on basket list
		String verifiedPrice = amazonPageObject.getProductPriceInCart();
		Thread.sleep(2000);

		//Price Verification
		if (productPrice.equalsIgnoreCase(verifiedPrice)) {
			System.out.println("Price verification successful!");
		}
		else {
			System.out.println("Price verification failed!");
		}
		Thread.sleep(2000);
	}
}
